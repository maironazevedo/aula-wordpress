<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V8ViyyUO42K6n4IUaBIw8txlJo5C7lgx6ID6SdhUlujqMwaCm8tCe952uzeMrnX+RTknkga0Nacbx9YIOz8EbA==');
define('SECURE_AUTH_KEY',  '/M3H0lP9/WK7fLVkf0xNWAkCnEpktVePwXtUbhvrvCDQ6R8k3fUrRXOFLvd5o8+DBy7eEEyhhIWDhLJSGY0pgw==');
define('LOGGED_IN_KEY',    'WEu7FFPTXpdr9jYbr5d4z5cxAxZBPwR+SGJ66tcsCMm8dZmDHYTzfONm/BZccrN8yn/CyhUjHWLBYcd4xk0VfQ==');
define('NONCE_KEY',        'mFCjXCrMwraTDA1F6E4iDJnEeGM8WUqQeY6mvgw/79JzF0kvIUHoID70lB4bpEQDPvwA4heBwBKNnV+PdudmkA==');
define('AUTH_SALT',        '8OgWV85aI922SVsiaiX8e6s5cCUzdGZD3KpsqrDrOE9sI8EKvvwzlr0Up/UMhO7rzktJS322ZodVK5wjHvLOYw==');
define('SECURE_AUTH_SALT', 'sqBLV+HDniIFqdPa3pVNdYOXtklP2O8dMb87iuALQcvcgHI+ZVmAi1/qOzjNAXrYs2Swe8gKYdu5uevU80ElZg==');
define('LOGGED_IN_SALT',   'aMEF6LLffs86dkXODu98yTp2MjBOZlDUdGKkpcIDbT7PADfsnp/v06ii9RR1gFFHO9dpD1LFsuhryOAFbdr+Cg==');
define('NONCE_SALT',       'qb8HLtqiF4Pg/RFQ2/QJCqqSzaqrARiccDn7jn510FDaX/EXw/XxXprcnX7Jl/TIgBWp6/WkuVEJlAob+qrgGA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
