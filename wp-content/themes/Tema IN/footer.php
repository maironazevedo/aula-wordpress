<footer>
    <div class="container">
        <div>
            <div id="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14700.77548294934!2d-43.13371!3d-22.906219!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfd5e35fb577af2f5!2sInstitute%20of%20Computing%20-%20UFF!5e0!3m2!1sen!2sus!4v1611023517676!5m2!1sen!2sus"
                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe><noscript>Ative o JavaScript para visualizar o mapa!</noscript></div>
            <ul>
                <li>Rua sei lá, 222, Cidade - Estado</li>
                <li>
                    <p>(99) 9999-9999</p>
                    <p>(99) 9999-9999</p>
                </li>
                <li><a href="mailto:email@email.com">email@email.com</a></li>
            </ul>
        </div>
        <div>
            <p>Desenvolvido por:</p>
            <a href="https://www.injunior.com.br"><img src="assets/img/logo-in-junior.png" alt="IN"></a>
        </div>
    </div>
    <div id="copyright">
        <p>Copyright &#169; 2021 - Tururu todos os direitos reservados</p>
    </div>
</footer>
</body>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/mailgo.min.js"></script>

</html>